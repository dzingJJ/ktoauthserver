/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.dzingishan.ktoauthserver.network.PacketManager;
import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;

/**
 * Class which decodes bytes to packet
 *
 * @author dzing
 */
public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() < 8) {
            return;
        }
        int reader = in.readerIndex();
        try {
            PacketFrame frame = new PacketFrame(in.readInt(), in.readInt());
            Packet p = PacketManager.getServerBoundPacket(frame);
            p.read(in);
            out.add(p);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            in.readerIndex(reader);
            return;
        }
    }

}
