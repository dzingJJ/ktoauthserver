/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver;

import com.dzingishan.ktoauthserver.network.InboundController;
import com.dzingishan.ktoauthserver.network.PacketDecoder;
import com.dzingishan.ktoauthserver.network.PacketEncoder;
import com.dzingishan.ktoauthserver.network.PacketManager;
import com.dzingishan.ktoauthserver.queue.Managers;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 *
 * @author dzing
 */
public class ServerStarter {

    private NioEventLoopGroup serverBossGroup, serverWorkerGroup;

    private int port = 5555;

    private ChannelFuture f;

    private PacketManager mng = new PacketManager(new Managers());

    public void start() {
        //Creates new EventLoopGroups: Boss and worker
        serverBossGroup = new NioEventLoopGroup();
        serverWorkerGroup = new NioEventLoopGroup();

        //New server
        ServerBootstrap b = new ServerBootstrap();
        //Groups Boss and Worker
        b.group(serverBossGroup, serverWorkerGroup)
                .channel(NioServerSocketChannel.class) //Sets channel type
                .childHandler(new ChannelInitializer<SocketChannel>() { //Creates ChannelInitializer and adds all handlers to it
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder()).addLast(new InboundController(mng));
                    }
                })
                //Options
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
        f = b.bind(port); //Bind server to given channel
        System.out.println("STARTED");
    }

    public static int players = 1;

    public static void main(String[] args) {
        if (args.length > 0) {
            players = Integer.parseInt(args[0]);
            System.out.println("STARTING SERVER WITH " + players);
        }
        new ServerStarter().start();
    }

}
