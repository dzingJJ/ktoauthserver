/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.queue;

/**
 *
 * @author dzing
 */
public class Server {

    public String address;
    public int port;

    public int serverNumber, gameNumber;

    public long lastPacket = System.currentTimeMillis();

    public ServerStatus status;

    public PlayerDetails[] players;

    public Server(String address, int port, int serverNumber, ServerStatus status) {
        this.address = address;
        this.port = port;
        this.serverNumber = serverNumber;
        this.status = status;
    }

    public Server() {
    }

}
