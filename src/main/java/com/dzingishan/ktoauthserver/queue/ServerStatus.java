/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.queue;

/**
 *
 * @author dzing
 */
public enum ServerStatus {

    WAITING_FOR_CONNECTION(0), IDLE(1), START_GAME(2), GAME(3);

    private int number;

    private ServerStatus(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

}
