//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package com.dzingishan.ktoauthserver.queue;

/**
 * Stores Unique ID Of Player, used for identification of player in game
 *
 * @author dzing
 */
public class PlayerDetails {

    public final String Nickname;
    public final int UniqueNumber;

    public PlayerDetails(String nickname, int uniqueNumber) {
        Nickname = nickname;
        UniqueNumber = uniqueNumber;
    }

    @Override
    public String toString() {
        return Nickname + ":" + UniqueNumber;
    }

}
