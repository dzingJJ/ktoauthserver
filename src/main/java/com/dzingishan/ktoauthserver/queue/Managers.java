/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.queue;

import com.dzingishan.ktoauthserver.ServerStarter;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P22LoginStatus;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P25GameFound;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P29StartTheGame;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P30Confirm;
import io.netty.channel.Channel;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author dzing
 */
public class Managers {

    //TODO: checking if player is in game
    public Map<Integer, Long> QueueWaiting = new ConcurrentHashMap<>();

    public Set<Server> Servers = ConcurrentHashMap.newKeySet();

    public Set<Integer> playersWaitingForServer = ConcurrentHashMap.newKeySet();

    public Set<Server> gameQueue = ConcurrentHashMap.newKeySet();

    public Map<Integer, Server> playersReadyToJoin = new ConcurrentHashMap<>();

    public SecureRandom random = new SecureRandom();

    private Timer timer = new Timer();

    public Managers() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                cleanServer();
                System.out.println("Currently " + Servers.size() + " servers are online");
                searchGames();
            }
        }, 0, 300);
    }

    private void cleanServer() {
        for (Server Server1 : Servers) {
            if (System.currentTimeMillis() - Server1.lastPacket > 3000) {
                System.out.println("Deleting server");
                Servers.remove(Server1);
            }
        }
    }

    public void QueueJoin(int uuid) {
        QueueWaiting.put(uuid, System.currentTimeMillis());
    }

    public void QueueLeft(int uuid) {
        QueueWaiting.remove(uuid);
    }

    public void queueCheck(int uuid, Channel channel) {
        System.out.println("Queue check");
        if (playersReadyToJoin.containsKey(uuid)) {
            System.out.println("FOUND GAME!");
            Server s = playersReadyToJoin.remove(uuid);
            P25GameFound found = new P25GameFound();
            found.address = s.address;
            found.port = s.port;
            channel.writeAndFlush(found);
        } else {
            QueueWaiting.put(uuid, System.currentTimeMillis());
            channel.writeAndFlush(new P30Confirm());
        }
    }

    //TODO: more games
    public void searchGames() {
        long milis = System.currentTimeMillis();
        QueueWaiting.entrySet().stream().filter((e) -> {
            return milis - e.getValue() > 5000;
        }).forEach((e) -> QueueWaiting.remove(e.getKey()));
        System.out.println(QueueWaiting.size() + " is searching for game");
        searchGameForPlayers(ServerStarter.players);//TODO
    }

    private Server getFreeServer() {
        for (Server Server1 : Servers) {
            if (Server1.status == ServerStatus.IDLE && (System.currentTimeMillis() - Server1.lastPacket) < 2000) {
                return Server1;
            }
        }
        return null;
    }

    private boolean searchGameForPlayers(int count) {
        if (QueueWaiting.keySet().stream().filter((o) -> {
            return !playersReadyToJoin.containsKey(o) && !playersWaitingForServer.contains(o);
        }).count() < count) {
            return false;
        }
        System.out.println("Searching Games");
        Server s = getFreeServer();
        System.out.println("Free server: " + s);
        if (s == null) {
            return false;
        }
        Integer[] arra = QueueWaiting.keySet().stream().filter((o) -> {
            return !playersReadyToJoin.containsKey(o) && !playersWaitingForServer.contains(o);
        }).limit(count).toArray(Integer[]::new);
        s.status = ServerStatus.START_GAME;
        PlayerDetails[] players = getPlayersDetails(arra);
        s.players = players;
        s.gameNumber = random.nextInt();
        gameQueue.add(s);
        for (Integer integer : arra) {
            playersWaitingForServer.add(integer);
        }
        return true;
    }

    public P22LoginStatus getPlayerData(String email, String pass) {
        return null; //TODO
    }

    public PlayerDetails[] getPlayersDetails(Integer[] players) {
        PlayerDetails[] pl = new PlayerDetails[players.length];
        for (int i = 0; i < pl.length; i++) {
            pl[i] = getPlayerDetails(players[i]);
        }
        return pl;
    }

    public PlayerDetails getPlayerDetails(int uuid) {
        //TODO
        PlayerDetails p = new PlayerDetails("Nickname_" + uuid, uuid);
        return p;
    }

    public int registerServer(String address, int[] ports) {
        int serverNumber = random.nextInt();
        System.out.println("Registering server" + address + " and " + Arrays.toString(ports));
        for (int port : ports) {
            Server s = new Server();
            s.address = address;
            s.port = port;
            s.status = ServerStatus.IDLE;
            s.serverNumber = serverNumber;
            Servers.add(s);
        }
        return serverNumber;
    }

    public void unregisterServer(int serverNumber) {
        Servers.stream().filter((o) -> o.serverNumber == serverNumber).forEach(Servers::remove);
    }

    private Server getServerByGameNumber(int id) {
        for (Server Server1 : Servers) {
            if (Server1.gameNumber == id) {
                return Server1;
            }
        }
        return null;
    }

    public void serverCheck(int serverNumber, Channel channel) {
        for (Server Server1 : Servers) {
            if (Server1.serverNumber == serverNumber) {
                Server1.lastPacket = System.currentTimeMillis();
            }
        }
        Server s = gameQueue.stream().filter((o) -> o.serverNumber == serverNumber).findFirst().orElse(null);
        if (s != null) {
            System.out.println("Found game for server, sending data");
            gameQueue.remove(s);
            P29StartTheGame p = new P29StartTheGame();
            p.gameNumber = s.gameNumber;
            p.players = s.players;
            p.port = s.port;
            channel.writeAndFlush(p);
        } else {
            channel.writeAndFlush(new P30Confirm());
        }

    }

    public void gameStart(int gameNumber, int serverNumber) {
        for (Server Server1 : Servers) {
            if (Server1.serverNumber == serverNumber && Server1.gameNumber == gameNumber) {
                Server1.status = ServerStatus.GAME;
                return;
            }
        }
    }

    public void gameFinish(int gameNumber, int serverNumber) {
        for (Server s : Servers) {
            if (s.serverNumber == serverNumber && s.gameNumber == gameNumber) {
                s.status = ServerStatus.IDLE;
                s.gameNumber = 0;
                s.players = null;
                return;
            }
        }
    }

    public void serverReady(int gameNumber) {
        Server s = getServerByGameNumber(gameNumber);
        for (PlayerDetails player : s.players) {
            playersReadyToJoin.put(player.UniqueNumber, s);
            playersWaitingForServer.remove(player.UniqueNumber);
        }
    }

}
