/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.managers;

import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author dzing
 */
public class TokensManager {

    public static Map<Integer, Token> tokenMap = new ConcurrentHashMap();

    public static SecureRandom random = new SecureRandom();

    public static int generateNewToken(int uuid) {
        Token token = new Token(uuid);
        tokenMap.put(uuid, token);
        System.out.println(token.token + " " + uuid);
        return token.token;
    }

    public static class Token {

        public int uuid, token;
        public long lastTime;

        public Token(int uuid) {
            this.uuid = uuid;
            this.token = random.nextInt();
            this.lastTime = System.currentTimeMillis();
        }
    }

    public static boolean isCorrectToken(int uuid, int token) {
        System.out.println("Correct: " + uuid + " " + token);
        if (isExpired(uuid)) {
            System.out.println("Expired token");
            return false;
        }
        Token t = tokenMap.get(uuid);
        return t != null && t.token == token;
    }

    public static boolean isExpired(int uuid) {
        Token t = tokenMap.get(uuid);
        if (t == null) {
            return true;
        }
        return (System.currentTimeMillis() - t.lastTime) > 600000;
    }

    public static boolean refreshToken(int uuid, int token) {
        if (!isCorrectToken(uuid, token)) {
            return false;
        }
        Token t = tokenMap.get(uuid);
        t.lastTime = System.currentTimeMillis();
        return true;
    }

}
