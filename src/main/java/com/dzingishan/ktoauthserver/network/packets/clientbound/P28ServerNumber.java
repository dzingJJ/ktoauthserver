/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.clientbound;

import com.dzingishan.ktoauthserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P28ServerNumber extends ClientBoundPacket {

    public P28ServerNumber() {
        super(28);
    }

    public int serverNumber;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(serverNumber);
    }

}
