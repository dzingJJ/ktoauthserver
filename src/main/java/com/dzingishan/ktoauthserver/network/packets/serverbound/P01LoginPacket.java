/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.PacketFrame;
import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P01LoginPacket extends ServerBoundPacket {

    public P01LoginPacket() {
        super(new PacketFrame(1));
    }

    public String email, password;

    @Override
    public void read(ByteBuf buff) {
        email = Packet.readString(buff);
        password = Packet.readString(buff);
    }

}
