/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P07ServerUnregister extends ServerBoundPacket {

    public P07ServerUnregister() {
        super(7);
    }

    public int serverNumber;

    @Override
    public void read(ByteBuf buff) {
        serverNumber = buff.readInt();
    }

}
