/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.clientbound;

import com.dzingishan.ktoauthserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktoauthserver.network.packets.Packet;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P25GameFound extends ClientBoundPacket {

    public P25GameFound() {
        super(25);
    }

    public String address;
    public int port;

    @Override
    public void write(ByteBuf buff) {
        Packet.writeString(buff, address);
        buff.writeInt(port);
    }

}
