/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.clientbound;

import com.dzingishan.ktoauthserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.queue.PlayerDetails;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P29StartTheGame extends ClientBoundPacket {

    public P29StartTheGame() {
        super(29);
    }

    public int port, gameNumber;
    public PlayerDetails[] players;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(port);
        buff.writeInt(gameNumber);
        buff.writeInt(players.length);
        for (PlayerDetails player : players) {
            Packet.writeString(buff, player.Nickname);
            buff.writeInt(player.UniqueNumber);
        }
    }

}
