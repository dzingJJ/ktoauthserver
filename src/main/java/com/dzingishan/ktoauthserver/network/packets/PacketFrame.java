/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets;

/**
 * Frame of the packet, used to identify packet
 *
 * @author dzing
 */
public class PacketFrame {

    public int number, length;

    public PacketFrame(int number) {
        this.number = number;
    }

    public PacketFrame(int number, int length) {
        this.number = number;
        this.length = length;
    }

    public PacketFrame() {
    }

}
