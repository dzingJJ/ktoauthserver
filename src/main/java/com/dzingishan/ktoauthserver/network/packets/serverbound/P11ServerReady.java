/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P11ServerReady extends ServerBoundPacket {

    public P11ServerReady() {
        super(11);
    }

    public int gameNumber;

    @Override
    public void read(ByteBuf buff) {
        gameNumber = buff.readInt();
    }

}
