/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.clientbound;

import com.dzingishan.ktoauthserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P26TokenRefresh extends ClientBoundPacket {

    public P26TokenRefresh() {
        super(26);
    }

    public boolean success;

    @Override
    public void write(ByteBuf buff) {
        buff.writeBoolean(success);
    }

}
