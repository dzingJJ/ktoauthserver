/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P09GameStart extends ServerBoundPacket {

    public P09GameStart() {
        super(9);
    }

    public int gameNumber, serverNumber;

    @Override
    public void read(ByteBuf buff) {
        gameNumber = buff.readInt();
        serverNumber = buff.readInt();
    }

}
