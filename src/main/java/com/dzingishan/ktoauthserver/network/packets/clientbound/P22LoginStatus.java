/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.clientbound;

import com.dzingishan.ktoauthserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P22LoginStatus extends ClientBoundPacket {

    public P22LoginStatus() {
        super(new PacketFrame(22));
    }

    public boolean success;
    public int token, uuid;
    public String username;

    @Override
    public void write(ByteBuf buff) {
        buff.writeBoolean(success).writeInt(token).writeInt(uuid);
        Packet.writeString(buff, username);
    }

    @Override
    public int getLength() {
        return 10;
    }

}
