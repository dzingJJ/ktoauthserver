/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P02QueueJoin extends ServerBoundPacket {

    public P02QueueJoin() {
        super(2);
    }

    public int token, uuid;

    @Override
    public void read(ByteBuf buff) {
        token = buff.readInt();
        uuid = buff.readInt();
    }

}
