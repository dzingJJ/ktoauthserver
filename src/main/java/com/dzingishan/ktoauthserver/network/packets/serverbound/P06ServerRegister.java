/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network.packets.serverbound;

import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P06ServerRegister extends ServerBoundPacket {

    public P06ServerRegister() {
        super(6);
    }

    public String address;

    public int[] ports;

    @Override
    public void read(ByteBuf buff) {
        address = Packet.readString(buff);
        int l = buff.readInt();
        ports = new int[l];
        for (int i = 0; i < ports.length; i++) {
            ports[i] = buff.readInt();

        }
    }

}
