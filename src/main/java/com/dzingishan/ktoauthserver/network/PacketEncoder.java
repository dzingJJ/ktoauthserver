/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network;

import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Used to code Packets into bytes
 *
 * @author dzing
 */
public class PacketEncoder extends MessageToByteEncoder<Packet> {

    @Override

    protected void encode(ChannelHandlerContext ctx, Packet msg, ByteBuf out) throws Exception {
        System.out.println("Sending packet: " + msg.getClass().getSimpleName());
        if (msg instanceof ServerBoundPacket) {
            throw new IllegalArgumentException(msg.getClass().getSimpleName() + " is ServerBoundPacket, you can't send it!");
        }
        //System.out.println("LOL");
        ByteBuf buff = Unpooled.buffer(1);
        buff.writeInt(msg.frame.number);
        //System.out.println("LOL2");
        msg.write(buff);
        byte[] bytes = buff.array();
        //System.out.println(Arrays.toString(bytes) + " " + msg.getClass().getSimpleName());
        out.writeInt(bytes.length).writeBytes(bytes);
    }

}
