/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktoauthserver.network;

import com.dzingishan.ktoauthserver.network.managers.TokensManager;
import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.PacketFrame;
import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P22LoginStatus;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P23QueueJoinAccept;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P24QueueLeftAccept;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P26TokenRefresh;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P27TokenError;
import com.dzingishan.ktoauthserver.network.packets.clientbound.P28ServerNumber;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P01LoginPacket;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P02QueueJoin;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P03QueueLeft;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P04QueueCheck;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P05RefreshToken;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P06ServerRegister;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P07ServerUnregister;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P08ServerOnlineTick;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P09GameStart;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P10GameFinish;
import com.dzingishan.ktoauthserver.network.packets.serverbound.P11ServerReady;
import com.dzingishan.ktoauthserver.queue.Managers;
import io.netty.channel.Channel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet Manger, used for getting correct packets and bridges between Game and
 * Server
 *
 * @author dzing
 */
public class PacketManager {

    private Managers managers;

    private final static Map<Integer, Class<? extends ServerBoundPacket>> SERVER_BOUND = new HashMap<>();
    //private final static Map<Integer, Class<? extends Packet>> LOGIN_BOUND = new HashMap<>();

    static {
        SERVER_BOUND.put(1, P01LoginPacket.class);
        SERVER_BOUND.put(2, P02QueueJoin.class);
        SERVER_BOUND.put(3, P03QueueLeft.class);
        SERVER_BOUND.put(4, P04QueueCheck.class);
        SERVER_BOUND.put(5, P05RefreshToken.class);
        SERVER_BOUND.put(6, P06ServerRegister.class);
        SERVER_BOUND.put(7, P07ServerUnregister.class);
        SERVER_BOUND.put(8, P08ServerOnlineTick.class);
        SERVER_BOUND.put(9, P09GameStart.class);
        SERVER_BOUND.put(10, P10GameFinish.class);
        SERVER_BOUND.put(11, P11ServerReady.class);
    }

    public PacketManager(Managers managers) {
        this.managers = managers;
    }

    /**
     * Gets empty ServeBound packet
     *
     * @param frame Frame to get packet based on
     * @return
     */
    public static Packet getServerBoundPacket(PacketFrame frame) {
        Class<? extends Packet> c = SERVER_BOUND.get(frame.number);
        if (c != null) {
            try {
                Packet p = c.newInstance();
                p.frame = frame;
                return p;
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(PacketManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Processing incomming Packet, bridges it between Login and Server manager
     *
     * @param packet Packet to process
     * @param channel Channel to get
     */
    public void processIncomingPacket(Packet packet, Channel channel) {
        if (packet instanceof P01LoginPacket) {
            //TODO
            P22LoginStatus status = new P22LoginStatus();
            status.success = true;
            status.uuid = TokensManager.random.nextInt();
            status.token = TokensManager.generateNewToken(status.uuid);
            status.username = "Player_" + TokensManager.random.nextInt(Integer.MAX_VALUE);
            channel.writeAndFlush(status);
        } else if (packet instanceof P02QueueJoin) {
            P02QueueJoin p = (P02QueueJoin) packet;
            if (!TokensManager.isCorrectToken(p.uuid, p.token)) {
                channel.writeAndFlush(new P27TokenError());
            } else {
                managers.QueueJoin(p.uuid);
                channel.writeAndFlush(new P23QueueJoinAccept());
            }
        } else if (packet instanceof P03QueueLeft) {
            P03QueueLeft p = (P03QueueLeft) packet;
            if (!TokensManager.isCorrectToken(p.uuid, p.token)) {
                channel.writeAndFlush(new P27TokenError());
            } else {
                managers.QueueLeft(p.uuid);
                channel.writeAndFlush(new P24QueueLeftAccept());
            }
        } else if (packet instanceof P04QueueCheck) {
            P04QueueCheck p = (P04QueueCheck) packet;
            if (!TokensManager.isCorrectToken(p.uuid, p.token)) {
                channel.writeAndFlush(new P27TokenError());
            } else {
                managers.queueCheck(p.uuid, channel);
            }
        } else if (packet instanceof P05RefreshToken) {
            P05RefreshToken p = (P05RefreshToken) packet;
            if (!TokensManager.refreshToken(p.uuid, p.token)) {
                channel.writeAndFlush(new P27TokenError());
            } else {
                channel.writeAndFlush(new P26TokenRefresh());
            }
        } else if (packet instanceof P06ServerRegister) {
            P06ServerRegister p = (P06ServerRegister) packet;
            int serverNumber = managers.registerServer(p.address, p.ports);
            P28ServerNumber pa = new P28ServerNumber();
            pa.serverNumber = serverNumber;
            channel.writeAndFlush(pa);
        } else if (packet instanceof P07ServerUnregister) {
            managers.unregisterServer(((P07ServerUnregister) packet).serverNumber);
        } else if (packet instanceof P08ServerOnlineTick) {
            managers.serverCheck(((P08ServerOnlineTick) packet).serverNumber, channel);
        } else if (packet instanceof P09GameStart) {
            managers.gameStart(((P09GameStart) packet).gameNumber, ((P09GameStart) packet).serverNumber);
        } else if (packet instanceof P10GameFinish) {
            managers.gameFinish(((P10GameFinish) packet).gameNumber, ((P10GameFinish) packet).serverNumber);
        } else if (packet instanceof P11ServerReady) {
            managers.serverReady(((P11ServerReady) packet).gameNumber);
        }
    }

}
