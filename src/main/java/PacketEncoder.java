/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.dzingishan.ktoauthserver.network.packets.Packet;
import com.dzingishan.ktoauthserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Used to code Packets into bytes
 *
 * @author dzing
 */
public class PacketEncoder extends MessageToByteEncoder<Packet> {

    @Override

    protected void encode(ChannelHandlerContext ctx, Packet msg, ByteBuf out) throws Exception {
        System.out.println("Sending packet: " + msg.getClass().getSimpleName());
        if (msg instanceof ServerBoundPacket) {
            throw new IllegalArgumentException(msg.getClass().getSimpleName() + " is ServerBoundPacket, you can't send it!");
        }
        out.writeInt(msg.frame.number);
        out.writeInt(msg.getLength());
        msg.write(out);
    }

}
